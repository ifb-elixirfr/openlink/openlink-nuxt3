import { NuxtAuthHandler } from "#auth";

export default NuxtAuthHandler({
  providers: [
    {
      id: "keycloak",
      name: "Keycloak",
      wellKnown:
        "https://login.igbmc.fr/realms/IGBMC-VALIDATION/.well-known/openid-configuration",
      type: "oauth",
      authorization: { params: { scope: "openid email profile" } },
      checks: ["pkce", "state"],
      idToken: true,
      options: {
        clientId: "openlink-nuxt",
        clientSecret: "HkW6maoGPzQebckO3zEzXd8wcHJFuiag",
        issuer: "https://login.igbmc.fr/realms/IGBMC-VALIDATION",
      },
      profile(profile) {
        var _profile$name;

        return {
          id: profile.sub,
          name:
            (_profile$name = profile.name) !== null && _profile$name !== void 0
              ? _profile$name
              : profile.preferred_username,
          email: profile.email,
          image: profile.picture,
        };
      },
    },
  ],
  callbacks: {
    async jwt({ token, account, profile }) {
      // login to OpenLink API with JWT token
      if (account) {
        const data = await $fetch("http://127.0.0.1:8000/login/", {
          method: "POST",
          headers: {
            Authorization: "Token " + account.id_token,
          },
        });
        token.openlinkToken = data.token;
      }
      return token;
    },
  },
});
