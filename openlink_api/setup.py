#!/usr/bin/env python

from setuptools import find_packages, setup

from openlink_api import __version__

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name='ifb-openlink',
    version=__version__,
    description='A dashboard that establish links between the structure of a research project and multiple data sources ',
    long_description=long_description,
    long_description_content_type="text/markdown",
    keywords='FAIR, open-data, data-managment-tools',
    url='https://gitlab.com/ifb-elixirfr/openlink/openlink-nuxt3',
    project_urls={
        'Bug Tracker': 'https://gitlab.com/ifb-elixirfr/openlink/openlink/-/issues',
        'Source Code': 'https://gitlab.com/ifb-elixirfr/openlink/openlink/',
    },
    author='Laurent Bouri, Julien Seiler, Mateo Hiriart, Nadia Goué',
    license='GNU General Public License v3 (GPLv3)',
    python_requires='>=3.8',
    packages=find_packages(),
    install_requires=[
        "bioblend==1.0.0",
        "django==4.1.4",
        "django-crispy-forms==1.14.0",
        "django-filter==22.1",
        "django-widget-tweaks==1.4.12",
        "django_extensions==3.2.1",
        "django_json_ld==0.0.5",
        "requests==2.28.1",
        "django-popup-forms==1.0.3",
        "wheel==0.38.4",
        "omero-py==5.13.1",
        "djangorestframework==3.14.0",
        "django-rq==2.6.0",
        "django-guardian==2.4.0",
        "django-ckeditor==6.5.1",
        "django-debug-toolbar==3.8.1",
        "paramiko==2.12.0",
        "django-environ==0.9.0",
        "hvac==1.0.2",
        "django-auth-ldap==4.1.0",
        "django-rest-knox==4.2.0",
        "psycopg2==2.9.5"
    ],
    entry_points={
        'console_scripts': [
            'openlink = openlink_api:manage',
        ],
    },
    include_package_data=True,
    classifiers=[
        'Programming Language :: Python :: 3',
        'Framework :: Django :: 3.0',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Topic :: Scientific/Engineering',
        'Topic :: System :: Systems Administration',
        'Topic :: Internet :: WWW/HTTP :: WSGI :: Application',
    ]
)
