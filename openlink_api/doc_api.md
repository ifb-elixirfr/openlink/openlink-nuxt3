# Openlink APi documentation

## Django rest framework api version 3.14

## List uri:  

#### Every url start with **api/**.  
#### Example for local server: 127.0.0.1:8000/api/login/


##Basic:
- **login/** 
  - **POST:** Login urls should automaticly redirect to ldap or oidc based on django settings.  
          Also authentified toward vault server.
          Ldap request data required 'login' and 'password".
          Return knox token needed for every other api call in header as autorized token
- **logout/**
  - **POST:** Logout user revoke knox token, vault token and delete server side session.  
- **user/**  
  - **GET:** Return yourself user information
- **profile/**
  - **GET:** Return yourself profile information
  
##Investigation:
- **investigation/** 
  - **GET:** Return list of all investigations
  - **POST:** Create investigation
              Request data: {"name": "can't be null", "description": "can be null"}
  
- **investigation/<int:pk>/**
  - **GET:**  Return information on request investigation id
  - **PUT:**  Modify requested investigation
  - **DELETE:** delete requested investigation
  
- **investigation/<int:pk>/tree/**
  - **GET:** Return information on requested investigation in tree form with detailled information for all manytomany field  
- **investigation/<int:pk>/members/**
  - **GET:** Return members list for requested investigation
  
- **investigation/<int:pk>/tools/** 
  - **GET:** Return tools list for requested investigation
  
- **investigation/<int:pk>/add/study/**
  - **POST:** Create study and add it to requested investigation

## Study
- **study/**
  - **GET:** Return list of all studies
  - **POST:** Create study
              Request data: "name", "description"
  
- **study/<int:pk>/** 
  - **GET:**  Return information on request study id
  - **PUT:**  Modify requested study
  - **DELETE:** delete requested study
  
- **study/<int:pk>/add/assay/**
  - **POST:** Create assay and add it to requested study

## Assay
- **assay/**
  - **GET:** Return list of all assays
  - **POST:** Create assay
              Request data: "name", "description"
  
- **assay/<int:pk>/** 
  - **GET:**  Return information on request assay id
  - **PUT:**  Modify requested assay
  - **DELETE:** delete requested assay

## IsaObject
- **isaobject/<int:isaobject_id>/mapping/**
  - **GET:** Return list of tool available for the request isaobject and user
  
- **isaobject/<int:isaobject_id>/mapping/tool/<int:tool_id>/** 
  - **GET:**  Return list of mappable object on highest level for the request tool

- **isaobject/<int:isaobject_id>/mapping/tool/<int:tool_id>/container/<str:container_id>/** 
  - **GET:**  Return list of mappable object that are in any container known by tool
              Exemple: return list of dataset for a given history in galaxy

- **isaobject/<int:isaobject_id>/mapping/tool/<int:tool_id>/datalink/**
  - **GET** Return list of all datalink for request isaobject
  - **POST** Map given datalink in request data to request isaobject

## Connectors
- **connectors/?type=(mapper/publisher)**
  - **GET:** Return list of available connector for the openlink server.
             Parameters: ?type=mapper or ?type=publisher

- **tool/**
  - **GET:** Return list of all tools  
  - **POST** Create tool example request data```json {  
                            "tool": {
                                "name":"galaxytest2", 
                                "connector":"GalaxyConnector"  
                            },  
                            "toolparam": {  
                                "url":"http://127.0.0.1:8080/",  
                                "login": "test",  
                                "password": "testpassword"  
                            },  
                            "async": "off"  
                        }```

- **tool/<int:pk>/**
  - **GET:** Return requested tool information
  - **PUT:** Modify requested tool  
  - **DELETE:** Delete requested tool  
                        