import inspect
import json
import logging
import sys
from abc import ABC, abstractmethod
from importlib import import_module

from crispy_forms.helper import FormHelper
from django import forms
from django.conf import settings

logger = logging.getLogger(__name__)

LIST_STRUCTURE = 0
TREE_STRUCTURE = 1


class ToolForm(forms.Form):
    project_id = int

    def __init__(self, *args, **kwargs):
        super(ToolForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.disable_csrf = True
        self.helper.form_group_wrapper_class = "row"
        self.helper.label_class = "col-sm-offset-1 col-sm-2"
        self.helper.field_class = "col-md-8"

    def clean(self):
        from openlink_api.core.models import Tool

        cleaned_data = super().clean()
        if "name" in cleaned_data:
            if Tool.objects.filter(
                name=cleaned_data["name"], project_id=self.project_id
            ).first():
                self.add_error("name", "a tool with the same name already exists ")
        return cleaned_data


class BasicObject:
    """Constructor method"""

    def __init__(self, id, name, inner_type=None, url=None, description=None, size=None):
        self.id = id
        self.name = name
        self.inner_type = inner_type
        self.url = url
        self.description = description
        self.size = size

    def get_size(self):
        return self.size


class DataObject(BasicObject):
    """Constructor method"""

    def __init__(self, id, name, inner_type=None, url=None, description=None, size=None):
        super().__init__(id, name, inner_type, url, description, size)


class ContainerObject(BasicObject):
    """Constructor method"""

    def __init__(self, id, name, inner_type=None, url=None, description=None, size=None):
        super().__init__(id, name, inner_type, url, description, size)


class ContainerDataObject(DataObject, ContainerObject):
    """Constructor method"""

    def __init__(self, id, name, inner_type=None, url=None, description=None, size=None):
        super().__init__(id, name, inner_type, url, description, size)


class ToolConnector(ABC):
    """Parent class of all connectors, declares the basic functions of a connector."""

    @classmethod
    @abstractmethod
    def get_name(cls):
        """Return the connector name.

        Returns:
            str: Connector name.
        """
        raise NotImplementedError()

    @classmethod
    @abstractmethod
    def get_type(cls):
        """Return connector type.

        Returns:
            str: Connector.super()
        """

    @classmethod
    @abstractmethod
    def get_form(cls):
        """Return dictionnary of required field for connection.

        Returns:
            str: {"url": "", "login": "", "password": ""}
        """

    @classmethod
    @abstractmethod
    def list_public_param(cls):
        """Return list of required field for connection that are public.

        Returns:
            ["url"]
        """

    @classmethod
    @abstractmethod
    def list_private_param(cls):
        """Return list of required field for connection that are private.

        Returns:
            ["login", "password"]
        """

    @classmethod
    @abstractmethod
    def has_access_url(cls):
        """Return True if the mapping object can be accessed online, through an URL.

        Returns:
            bool: True if the mapping object be can be accessed through an URL.
        """
        raise NotImplementedError()

    @classmethod
    @abstractmethod
    def get_logo(cls):
        """Return the static path of the connector logo.

        Returns:
            str: Connector logo static path.
        """
        raise NotImplementedError()

    @classmethod
    @abstractmethod
    def get_color(cls):
        """Return the color of the tool.

        Returns:
            str: Connector color.
        """
        raise NotImplementedError()

    @classmethod
    @abstractmethod
    def test_connection(cls):
        """Test if connection is possible with given credentials.

        Returns:
            HTTP code response.
        """
        raise NotImplementedError()

    @classmethod
    def has_mapping_options(cls):
        """Return True if the connector has mapping options.

        Returns:
            bool: True if connector has specific mapping options.
        """
        raise NotImplementedError()

    @abstractmethod
    def get_url_link_to_an_object(self, obj_type, obj_id):
        """Get url link for a given object id and type.

        Arguments:
            obj_type (str): type of the object
            obj_id (int): Id of an object
        Returns:
            str:
            url for a given object
        """
        raise NotImplementedError()


class Mapper(ToolConnector):
    """Parent class for data management connector, declares the minimum
    functions to map a data to openlink.
    """

    @abstractmethod
    def get_data_root(self):
        """Return list of all mappable object from highest level

        [
            {
                "name": "",
                "id": ""
            },
            {
                "name": "",
                "id": ""
            }
        ]

        """
        raise NotImplementedError()

    @abstractmethod
    def get_child_data(self, container):
        """Return list of all mappable object from a given container

        [
            {
                "name": "",
                "id": ""
            },
            {
                "name": "",
                "id": ""
            }
        ]

        """
        raise NotImplementedError()

    @abstractmethod
    def download(self, object_id, path):
        """Download data from an object link in openlink path.

        Arguments:
           object_id (str): Id of an object.
           path (str): Path to where the downloaded files will be stored.
        """
        raise NotImplementedError()

    @abstractmethod
    def check_file_access(self, object_id):
        """Check data acces from an object id.

        Arguments:
           object_id (str): Id of an object.
        """
        raise NotImplementedError()


class Publisher(ToolConnector):
    """Parent class for data publishing connectors, declares the minimum
    functions to publish a data.
    This class can be modified after each new publisher added.
    """

    @abstractmethod
    def create_empty_depo(self):
        """Initiate a new depository in the publisher.

        Returns:
            json: Json structure sent by api connector requests.
        """
        raise NotImplementedError()

    @abstractmethod
    def add_file_to_depo(self, json, path_to_file):
        """adding file in depository

        Arguments:
            json (json): Json structure sent by api connector requests.
            path_to_file (str): path to the file downloaded by Openlink.
        Returns:
            json: Json structure sent by api connector requests.
        """
        raise NotImplementedError()

    @abstractmethod
    def add_metadata_to_depo(self, meta, jsonr):
        """adding metadata to depository

        Arguments:
            meta (dict): Dictionary of metadata specific of an object from a connector.
            jsonr (str): link to the depo, retrieved from a json object.
        Returns:
            json: Json structure sent by api connector requests.
        """
        raise NotImplementedError()

    @abstractmethod
    def publish_depo(self, json):
        """publish the depository.

        Arguments:
            json (json): Json structure holding a usefull link to publish data.
        Returns:
            json: Json structure sent by api connector requests.
        """
        raise NotImplementedError()


class AuthentificationError(Exception):
    def __init__(self, tool, invalid_item):
        self.message = "invalid " + tool.get_name() + " " + invalid_item
        super().__init__(self.message)


class NotFoundError(Exception):
    def __init__(self, tool):
        self.message = "the " + tool.get_name + " object not found "
        super().__init__(self.message)


class ToolUnreachableError(Exception):
    def __init__(self, tool):
        self.message = (
            tool.get_name() + " server temporarily unavailable, try again later "
        )
        super().__init__(self.message)


class permissionError(Exception):
    def __init__(self, tool):
        self.message = "you do not have the necessary permission for this action"
        super().__init__(self.message)


class defaultError(Exception):
    def __init__(self, tool):
        self.message = "an error has occured on " + tool.get_name()
        super().__init__(self.message)


def find_connectors_in_module(module):
    connectors = []
    for name, obj in inspect.getmembers(module):
        if (
            inspect.isclass(obj)
            and issubclass(obj, ToolConnector)
            and obj is not ToolConnector
            and obj is not Publisher
            and obj is not Mapper
        ):
            connectors.append(obj)
        if inspect.ismodule(obj) and module.__name__ in obj.__name__:
            connectors.extend(find_connectors_in_module(obj))

    return connectors


def get_connectors():
    """Retrieves dynamically all implementation of connectors in apps"""
    connectors = []

    for app_module_name in settings.INSTALLED_APPS:
        connectors_module = None
        connectors_module_name = f"{app_module_name}.connectors"
        if connectors_module_name in sys.modules:
            connectors_module = sys.modules[connectors_module_name]
        else:
            try:
                connectors_module = import_module(connectors_module_name)
            except ModuleNotFoundError:
                pass
                # logger.debug(f'No connectors module in {app_module_name}')
        connectors.extend(find_connectors_in_module(connectors_module))

    return connectors


def get_connector_class(connector_name):
    for connector in get_connectors():
        if connector.__name__ == connector_name:
            return connector

    return None


def get_connector(tool, token, user_vault_id=None):
    """
    Return instance of the connector corresponding to the given tool instance
    """
    connector_class = get_connector_class(tool.connector)
    if user_vault_id:
        return connector_class(tool, token=token, user_vault_id=user_vault_id)
    elif token and user_vault_id is None:
        return connector_class(tool=tool, token=token)

    else:
        return connector_class(tool=tool, token=None)
