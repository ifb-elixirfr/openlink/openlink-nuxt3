import logging
import os
from datetime import datetime

import hvac
from ckeditor.fields import RichTextField
from django.conf import settings
from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.forms import ModelChoiceField
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils import timezone
from openlink_api.core.connector import get_connector
from redis import Redis
from rq.exceptions import NoSuchJobError
from rq.job import Job

logger = logging.getLogger(__name__)

vault_url = (
    settings.AUTH_VAULT_CONFIG["VAULT_HOSTNAME"]
    + settings.AUTH_VAULT_CONFIG["VAULT_PORT"]
)


class Team(models.Model):
    name = models.CharField(max_length=100, blank=True)
    date = models.DateTimeField(default=datetime.now, blank=True, null=True)

    def __str__(self):
        return self.name


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    team = models.ManyToManyField(Team, blank=True)
    vault_id = models.CharField(max_length=100, blank=True)
    tool_list = models.ManyToManyField(
        "Tool", through="Tool_linked_user", related_name="tool_list"
    )
    tool_list_shared = models.ManyToManyField(
        "Tool", through="Tool_shared_linked_user", related_name="tool_list_shared"
    )

    def __str__(self):
        return str(self.user)


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()


class Mappableobject(models.Model):
    def __str__(self):
        return str(self.id)

    @property
    def type(self):
        return self._meta.model_name

    @classmethod
    def create(cls, form, profile, parent_id):
        print("hello")
        logger.debug("create")
        item = form.save(commit=False)
        item.author = profile
        item.date_created = timezone.now()
        item.save()

        parent_type = cls.get_parent_type()
        parent_item = getattr(parent_type, "get_" + parent_type.__name__.lower())(
            parent_id
        ).first()

        getattr(parent_item, item.get_plural()).add(item)
        parent_item.save()

        return item


class Image(Mappableobject):
    name = models.CharField(max_length=1000, blank=True)
    author = models.ForeignKey(Profile, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.name


class Data(Mappableobject):
    name = models.CharField(max_length=1000)
    description = RichTextField(
        blank=True, null=True, config_name="zenodo"
    )  # models.TextField(blank=True, null=True)
    author = models.ForeignKey(Profile, on_delete=models.CASCADE, null=True)
    date_created = models.DateTimeField(blank=True, null=True)
    date_modified = models.DateTimeField(blank=True, null=True),

    def __str__(self):
        return self.name

    def get_plural(self):
        return "datas"

    @staticmethod
    def get_parent(id):
        return Assay.objects.filter(datas__id=id)

    def get_data(id):
        return Data.objects.filter(id=id)

    def get_assay(id):
        id = id.id if type(id) == Data else id
        return Assay.objects.filter(datas__id=id)

    def get_study(id):
        id = id.id if type(id) == Data else id
        return Study.objects.filter(assays__datas__id=id)

    def get_investigation(id):
        id = id.id if type(id) == Data else id
        return Investigation.objects.filter(studies__assays__datas__id=id)

    def get_parent_type():
        return Assay

    def get_child_type():
        return None

    def model_plural():
        return "datas"

    def get_absolute_url(self):
        return reverse(
            "core:edit-data",
            kwargs={
                "project_id": self.get_project().first().id,
                "investigation_id": self.get_investigation().first().id,
                "data_id": self.id,
            },
        )

    @classmethod
    def create_instance(cls, child_data_object, author):
        New_data = cls()
        New_data.name = child_data_object.name
        New_data.description = child_data_object.description
        New_data.author = author
        New_data.date_created = timezone.now()
        return New_data


class Assay(Mappableobject):
    name = models.CharField(max_length=1000)
    description = RichTextField(
        blank=True, null=True, config_name="zenodo"
    )  # models.TextField(blank=True, null=True)
    author = models.ForeignKey(Profile, on_delete=models.CASCADE, null=True)
    date_created = models.DateTimeField(blank=True, null=True)
    date_modified = models.DateTimeField(blank=True, null=True)
    datas = models.ManyToManyField(Data, blank=True)

    def __str__(self):
        return self.name

    def get_plural(self):
        return "assays"

    @staticmethod
    def get_parent(id):
        return Study.objects.filter(assays__id=id)

    def get_data(id):
        return Data.objects.filter(assay__id=id)

    def get_assay(id):
        return Assay.objects.filter(id=id)

    def get_study(id):
        id = id.id if type(id) == Assay else id
        return Study.objects.filter(assays__id=id)

    def get_investigation(id):
        id = id.id if type(id) == Assay else id
        return Investigation.objects.filter(studies__assays__id=id)

    @property
    def items(self):
        return list(self.datas.all())

    def get_parent_type():
        return Study

    def get_child_type():
        return Data

    def model_plural():
        return "assays"

    def get_absolute_url(self):
        return reverse(
            "core:edit-assay",
            kwargs={
                "project_id": self.get_project().first().id,
                "investigation_id": self.get_investigation().first().id,
                "assay_id": self.id,
            },
        )

    @classmethod
    def create_instance(cls, child_data_object, author):
        New_assay = cls()
        New_assay.name = child_data_object.name
        New_assay.description = child_data_object.description
        New_assay.author = author
        New_assay.date_created = timezone.now()
        return New_assay

    def link_child_to_parent(parent_data_object, child_data_object):
        parent_data_object.datas.add(child_data_object)


class Study(Mappableobject):
    name = models.CharField(max_length=1000)
    description = RichTextField(
        blank=True, null=True, config_name="zenodo"
    )  # models.TextField(blank=True, null=True)
    author = models.ForeignKey(
        Profile, on_delete=models.CASCADE, null=True, related_name="studies"
    )
    date_created = models.DateTimeField(blank=True, null=True)
    date_modified = models.DateTimeField(blank=True, null=True)
    assays = models.ManyToManyField(Assay, blank=True)

    def __str__(self):
        return self.name

    @property
    def items(self):
        return list(self.assays.all())

    def get_plural(self):
        return "studies"

    @staticmethod
    def get_parent(id):
        return Investigation.objects.filter(studies__id=id)

    def get_data(id):
        return Data.objects.filter(assay__study__id=id)

    def get_assay(id):
        return Assay.objects.filter(study__id=id)

    def get_study(id):
        return Study.objects.filter(id=id)

    def get_investigation(id):
        id = id.id if type(id) == Study else id
        return Investigation.objects.filter(studies__id=id)

    def get_parent_type():
        return Investigation

    def get_child_type():
        return Assay

    def model_plural():
        return "studies"

    def get_absolute_url(self):
        return reverse(
            "core:edit-study",
            kwargs={
                "project_id": self.get_project().first().id,
                "investigation_id": self.get_investigation().first().id,
                "study_id": self.id,
            },
        )

    @classmethod
    def create_instance(cls, child_data_object, author):
        New_study = cls()
        New_study.name = child_data_object.name
        New_study.description = child_data_object.description
        New_study.author = author
        New_study.date_created = timezone.now()
        return New_study

    def link_child_to_parent(parent_data_object, child_data_object):
        parent_data_object.assays.add(child_data_object)


class Investigation(Mappableobject):
    name = models.CharField(max_length=1000)
    description = RichTextField(
        blank=True, null=True, config_name="zenodo"
    )  # models.TextField(blank=True, null=True)
    author = models.ForeignKey(Profile, on_delete=models.CASCADE, null=True)
    date_created = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    date_modified = models.DateTimeField(blank=True, null=True)
    studies = models.ManyToManyField(Study, blank=True)

    class Meta:
        permissions = (("manage_user", "Manage user"), ("manage_tool", "Manage tool"))

    def __str__(self):
        return self.name

    @property
    def items(self):
        return list(self.studies.all())

    def get_plural(self):
        return "investigations"

    def get_data(id):
        return Data.objects.filter(assay__study__investigation__id=id)

    def get_assay(id):
        return Assay.objects.filter(study__investigation__id=id)

    def get_study(id):
        return Study.objects.filter(investigation__id=id)

    def get_investigation(id):
        return Investigation.objects.filter(id=id)

    def get_tool_list(id):
        return Tool.objects.filter(investigation=id)

    def get_child_type():
        return Study

    def model_plural():
        return "investigations"

    @classmethod
    def create_instance(cls, child_data_object, author):
        New_investigation = cls()
        New_investigation.name = child_data_object.name
        New_investigation.description = child_data_object.description
        New_investigation.author = author
        New_investigation.date_created = timezone.now()
        return New_investigation

    def link_child_to_parent(parent_data_object, child_data_object):
        parent_data_object.studies.add(child_data_object)


class MappingInvestigationUser(models.Model):
    ADMINISTRATOR = "administrator"
    CONTRIBUTOR = "contributor"
    COLLABORATOR = "collaborator"
    STATUS = [
        (ADMINISTRATOR, "administrator"),
        (CONTRIBUTOR, "contributor"),
        (COLLABORATOR, "collaborator"),
    ]
    user = models.ForeignKey(Profile, on_delete=models.CASCADE, null=True)
    investigation = models.ForeignKey(Investigation, on_delete=models.CASCADE, null=True)
    role = models.CharField(
        max_length=13,
        choices=STATUS,
        default=ADMINISTRATOR,
    )

    def __str__(self):
        return str(self.investigation) + "_" + str(self.user)


class Tool(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    connector = models.CharField(max_length=100, blank=True, null=True)
    author = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    date_created = models.DateTimeField(blank=True, null=True)
    investigation = models.ForeignKey(Investigation, on_delete=models.CASCADE, null=True)
    user_list = models.ManyToManyField(
        Profile, through="Tool_linked_user", related_name="user_list"
    )

    def __str__(self):
        return str(self.id)

    def get_list_user_linked(self):
        return self.user_list

    def get_public_param(self, key):
        value = Toolparam.objects.values_list("value", flat=True).get(
            key=key, tool_id_id=str(self.id)
        )
        return value

    def get_private_param(self, key, token, user_vault_id=None):
        if token is not None:
            try:
                vault_client = hvac.Client(url=vault_url, token=token)
                vault_client.kv.default_kv_version = 1
                reponse = vault_client.lookup_token()
                if user_vault_id:
                    path = os.path.join("shared", user_vault_id, str(self.id))
                else:
                    path = os.path.join(reponse["data"]["entity_id"], str(self.id))
                value = vault_client.secrets.kv.v1.read_secret(
                    mount_point="openlink", path=path
                )
                return value["data"][key]
            except Exception as e:
                raise e
        else:
            return None

    def get_connector(self, token, user_vault_id=None):
        return get_connector(self, token, user_vault_id)

    @property
    def tag_tool(self):
        connector = self.get_connector(None)
        tool_tag = connector.get_name()
        return tool_tag


class Tool_linked_user(models.Model):
    tool = models.ForeignKey(Tool, on_delete=models.CASCADE)
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)


class Tool_shared_linked_user(models.Model):
    tool = models.ForeignKey(Tool, on_delete=models.CASCADE)
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)


class Toolparam(models.Model):
    tool_id = models.ForeignKey(Tool, on_delete=models.CASCADE, null=True)
    key = models.CharField(max_length=100, blank=True, null=True)
    value = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return "%s" % self.tool_id

    def save(self, *args, **kwargs):
        super(Toolparam, self).save(*args, **kwargs)


class ToolparamChoiceField(ModelChoiceField):
    def label_from_instance(self, obj):
        return obj.value


class Mapping(models.Model):
    name = models.CharField(max_length=1000, blank=True)
    tool_id = models.ForeignKey(Tool, on_delete=models.CASCADE, blank=True, null=True)
    type = models.CharField(max_length=100)
    target_ressource = models.CharField(max_length=1000, blank=True, null=True)
    size = models.BigIntegerField(blank=True, null=True)
    author = models.ForeignKey(Profile, on_delete=models.CASCADE, null=True)
    foreign_id_obj = models.ForeignKey(
        Mappableobject, on_delete=models.CASCADE, null=True
    )

    def __str__(self):
        return self.name

    @classmethod
    def create_instance(cls, openlink_object, data_object, tool, author, size):
        mapping_object = Mapping()
        mapping_object.name = str(data_object.name)
        mapping_object.tool_id = tool
        mapping_object.type = type(openlink_object).__name__.lower()
        mapping_object.object_id = str(data_object.id)
        if type(openlink_object) == Data:
            mapping_object.size = size
        mapping_object.author = author
        mapping_object.foreign_id_obj = openlink_object
        return mapping_object

    @property
    def link_name(self):
        tool_qs = get_object_or_404(Tool, id=self.tool_id.id)
        connector = tool_qs.get_connector(None)
        try:
            object_info = connector.get_object(self.object_id, self.type)
            return object_info["name"]
        except Exception:
            return None

    @property
    def link_url(self):
        tool_qs = get_object_or_404(Tool, id=self.tool_id.id)
        connector = tool_qs.get_connector(None)
        return connector.get_url_link_to_an_object(self.type, self.object_id)

    @property
    def link_tool(self):
        tool_qs = get_object_or_404(Tool, id=self.tool_id.id)
        tool_name = tool_qs.name
        return tool_name

    @property
    def tag_tool(self):
        tool_qs = get_object_or_404(Tool, id=self.tool_id.id)
        connector = tool_qs.get_connector(None)
        tool_tag = connector.get_name
        return tool_tag

    @property
    def connector_class(self):
        tool_qs = get_object_or_404(Tool, id=self.tool_id.id)
        return tool_qs.get_connector(None)

    @property
    def get_supported_types_plural(self):
        from openlink.core.views import items

        data_type = self.type
        connector = self.connector_class
        ressource = connector.get_supported_types()
        ressources = {}
        i = 0
        for key in ressource:
            list_res_plural = []
            if key == data_type:
                i = 1
            else:
                if i == 1:
                    for value in ressource[key]:
                        list_res_plural.append(connector.get_plural_form_of_type(value))
                    ressources[items.get_plural_form_of_type(key)] = list_res_plural
        return ressources

    @property
    def get_supported_types(self):
        return self.connector_class.get_supported_types()


class MappingParam(models.Model):
    mapping_id = models.ForeignKey(Mapping, on_delete=models.CASCADE, null=True)
    key = models.CharField(max_length=100, blank=True, null=True)
    value = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return "%s %s" % (self.key, self.mapping_id)


class TaskInfo(models.Model):
    taskid = models.CharField(max_length=100, blank=True, null=True)
    description = models.CharField(max_length=1000, blank=True, null=True)
    status = models.CharField(max_length=100, blank=True, null=True)
    result = models.CharField(max_length=100, blank=True, null=True)
    target = models.ForeignKey(
        Mappableobject,
        on_delete=models.CASCADE,
        null=True,
        related_name="%(class)s_as_target",
    )
    author = models.ForeignKey(Profile, on_delete=models.CASCADE, null=True)
    investigation = models.ForeignKey(
        Investigation, on_delete=models.CASCADE, null=True, related_name="project_related"
    )
    start_date = models.DateTimeField(blank=True, null=True)
    end_date = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return str(self.taskid)

    @property
    def get_error(self):
        if self.status == "error":
            redis = Redis()
            try:
                job = Job.fetch(self.taskid, connection=redis)
                if job.exc_info is not None:
                    return str(
                        job.exc_info.split("Exception")[-1]
                        .split("Error")[-1]
                        .replace("\n", "")
                        .replace(":", "")
                    )
                else:
                    return "None"
            except NoSuchJobError:
                return "None"
        else:
            return ""

    @property
    def get_investigation_name(self):
        return str(self.investigation.name)

    @property
    def get_author_name(self):
        return str(self.author.user.username)

    @property
    def get_target_name(self):
        for type_class in Mappableobject.__subclasses__():
            try:
                name = type_class.objects.get(mappableobject_ptr=self.target)
            except type_class.DoesNotExist:
                pass
        return name

    @property
    def get_target_url(self):
        for type_class in Mappableobject.__subclasses__():
            try:
                url = type_class.objects.get(
                    mappableobject_ptr=self.target
                ).get_absolute_url()
            except type_class.DoesNotExist:
                pass
        return url
