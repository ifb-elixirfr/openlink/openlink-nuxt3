from openlink_api.core import models
from rest_framework import serializers
from openlink_api.core.api.serializers import base, connectors


class DatalinkSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.DataLink
        fields = ("id", "name", "tool_id", "target_ressource", "target_url", "size", "author", "publication")


class AssaySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Assay
        fields = ('id', 'name', 'description', 'author', 'date_created', 'date_modified', 'datalink')


class StudySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Study
        fields = ('id', 'name', 'description', 'author', 'date_created', 'date_modified', 'datalink', 'assays')


class InvestigationSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Investigation
        fields = ('id', 'name', 'description', 'author', 'date_created', 'date_modified', 'datalink', 'members', 'tools', 'studies')


class IsaObjectSerializer(serializers.ModelSerializer):
    datalink = DatalinkSerializer(many=True)
    class Meta:
        model = models.ISAObject
        fields = ('id', 'name', 'description', 'author', 'date_created', 'date_modified', 'datalink')

"""
Serializers class with nested relationships to disaplay full tree from investigation
"""


class DatalinkSerializerTree(serializers.ModelSerializer):
    class Meta:
        model = models.DataLink
        fields = ("id", "name", "tool_id", "target_ressource", "target_url", "size", "author", "publication")


class AssaySerializerTree(serializers.ModelSerializer):
    datalink = DatalinkSerializerTree(many=True)

    class Meta:
        model = models.Assay
        fields = ('id', 'name', 'description', 'author', 'date_created', 'date_modified', 'datalink')


class StudySerializerTree(serializers.ModelSerializer):
    assays = AssaySerializerTree(many=True)
    datalink = DatalinkSerializerTree(many=True)

    class Meta:
        model = models.Study
        fields = ('id', 'name', 'description', 'author', 'date_created', 'date_modified', 'datalink', 'assays')


class InvestigationSerializerTree(serializers.ModelSerializer):
    studies = StudySerializerTree(many=True)
    members = base.MemberSerializer(many=True)
    tools = connectors.ToolSerializer(many=True)
    datalink = DatalinkSerializerTree(many=True)

    class Meta:
        model = models.Investigation
        fields = ('id', 'name', 'description', 'author', 'date_created', 'date_modified', 'datalink', 'members', 'tools', 'studies')
