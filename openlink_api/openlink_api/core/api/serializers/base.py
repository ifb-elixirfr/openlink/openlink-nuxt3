from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from openlink_api.core import models
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username')


class LoginUserSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField()

    def validate(self, data):
        user = authenticate(**data)
        if user and user.is_active:
            return user
        raise serializers.ValidationError("Invalid Details.")


class ProfileSerializer(serializers.ModelSerializer):
    user = serializers.StringRelatedField(read_only=True)

    class Meta:
        model = models.Profile
        fields = ('user', 'vault_id', "email")


class MemberSerializer(serializers.ModelSerializer):
    profile = serializers.StringRelatedField(read_only=True)

    class Meta:
        model = models.Member
        fields = ('id', 'profile', 'permission')


class ContainerDataObjectSerializer(serializers.Serializer):
    id = serializers.CharField()
    name = serializers.CharField()
    inner_type = serializers.CharField()
    url = serializers.CharField()
    description = serializers.CharField()
    size = size = serializers.FloatField()