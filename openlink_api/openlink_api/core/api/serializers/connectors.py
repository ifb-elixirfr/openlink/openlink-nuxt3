from openlink_api.core import models
from rest_framework import serializers


class ToolSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Tool
        fields = ('id', 'name', 'connector', 'author', 'date_created')


class ToolparamSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Toolparam
        fields = ('tool_id', 'key', 'value')


class Tool_state_userSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ToolStateProfile
        fields = ('profile', 'tool', 'asyncTask')
