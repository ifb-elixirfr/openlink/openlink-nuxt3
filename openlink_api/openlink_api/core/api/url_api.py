from django.urls import path
from openlink_api.core.api.views import api_login, investigations, studies, assays, connectors, utils, isaobject
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [
    path('login/', api_login.LoginAPI_LDAP.as_view()),
    path('user/', api_login.UserAPI.as_view()),
    path('logout/', api_login.LogoutAPI.as_view()),

    path('profile/', utils.List_all_ProfileAPI.as_view()),

    ### Investigations
    path('investigation/', investigations.InvestigationAPI.as_view()), #get, post
    path('investigation/<int:pk>/', investigations.InvestigationAPI_id.as_view()),#get, put, delete
    path('investigation/<int:pk>/tree/', investigations.InvestigationAPI_tree.as_view()),#get
    path('investigation/<int:pk>/members/', investigations.InvestigationAPI_members.as_view()),#get
    path('investigation/<int:pk>/tools/', investigations.InvestigationAPI_tools.as_view()),#get
    # path('investigation/<int:pk>/data_distribution/', investigations.InvestigationAPI_dataDistribution.as_view()),#get /?by="tool","study"
    # path('investigation/<int:pk>/publications/', investigations.InvestigationAPI.as_view()),#get, post

    path('investigation/<int:pk>/add/study/', investigations.Investigation_add_study.as_view()), #post
    path('study/<int:pk>/add/assay/', studies.Study_add_assay.as_view()), #post

    ### Study
    path('study/', studies.StudiesListAPI.as_view()), #get, post
    path('study/<int:pk>/', studies.StudiesAPI.as_view()), #get, put, delete
    # path('study/<int:pk>/data_distribution', studies.StudiesAPI.as_view()),

    ### Assay
    path('assay/', assays.AssaysListAPI.as_view()),
    path('assay/<int:pk>/', assays.AssaysAPI.as_view()),
    # path('assay/<int:pk>/data_distribution', assays.AssaysAPI.as_view()),

    # Isa Object
    path('isaobject/<int:isaobject_id>/mapping/', isaobject.IsaObjectAPI.as_view()), #get
    path('isaobject/<int:isaobject_id>/mapping/tool/<int:tool_id>/', isaobject.IsaObject_MappingAPI.as_view()), #get
    path('isaobject/<int:isaobject_id>/mapping/tool/<int:tool_id>/container/<str:container_id>/', isaobject.IsaObject_Mapping_childAPI.as_view()), #get
    path('isaobject/<int:isaobject_id>/mapping/tool/<int:tool_id>/datalink/', isaobject.IsaObject_DatalinkAPI.as_view()), #get, post

    ## Connectors
    path('connectors/', connectors.Connectors.as_view()),
    path('tool/', connectors.toolsListAPI.as_view()),
    path('tool/<int:pk>/', connectors.toolsAPI.as_view()),


]

urlpatterns = format_suffix_patterns(urlpatterns)