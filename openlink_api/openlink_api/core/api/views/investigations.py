from guardian.shortcuts import assign_perm

from openlink_api.core.api.serializers import isa
from openlink_api.core import models
from openlink_api.core.api.views.api_login import get_current_user_profile
from rest_framework import permissions
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.shortcuts import get_object_or_404

from django.http import HttpResponse, Http404

"""
url:investigation/
GET: Return of all Investigation

POST: Create new investigation and add MappingInvestigationUser for request.user as administarator
"""


class InvestigationAPI(APIView):
    permission_classes = [permissions.IsAuthenticated, ]
    serializer_class = isa.InvestigationSerializer

    def get(self, request):
        investigations = models.Investigation.objects.all()
        investigations_serialized = isa.InvestigationSerializer(investigations, many=True)
        return Response(investigations_serialized.data)

    def post(self, request, format=None):
        serializer = isa.InvestigationSerializer(data=request.data)
        if serializer.is_valid():
            investigation = serializer.save(author=get_current_user_profile(request.user))

            new_member = models.Member()
            new_member.permission = "admin"
            new_member.profile = get_current_user_profile(request.user)
            new_member.save()

            investigation.members.add(new_member)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


"""
url:investigation/<int:pk/>
GET: Return details about requested investigation based on it's ID

PUT: Modified requested investigation based on it's ID with body content

DELETE: Delete requested investigation based on it's ID
"""


class InvestigationAPI_id(APIView):
    permission_classes = [permissions.IsAuthenticated, ]
    serializer_class = isa.InvestigationSerializer

    def get_object(self, pk):
        try:
            return models.Investigation.objects.get(pk=pk)
        except models.Investigation.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = isa.InvestigationSerializer(snippet)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        print(request.data)
        snippet = self.get_object(pk)
        if "name" not in request.data and snippet.name is not None:
            request.data["name"] = snippet.name
        serializer = isa.InvestigationSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

"""
url:investigation/<int:pk>/tree/

GET: Return all tree from an investigation ID
"""


class InvestigationAPI_tree(APIView):
    permission_classes = [permissions.IsAuthenticated, ]
    serializer_class = isa.InvestigationSerializerTree

    def get_object(self, pk):
        try:
            return models.Investigation.objects.get(pk=pk)
        except models.Investigation.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = isa.InvestigationSerializerTree(snippet)
        return Response(serializer.data)


"""
url:investigation/<int:pk>/datadistribution/

GET: Return DataDistribution for specified investigation
"""


class InvestigationAPI_dataDistribution(APIView):
    permission_classes = [permissions.IsAuthenticated, ]
    serializer_class = isa.InvestigationSerializer

    def get_object(self, pk):
        try:
            return models.Investigation.objects.get(pk=pk)
        except models.Investigation.DoesNotExist:
            raise Http404


"""
url:investigation/<int:pk>/members/

GET: Manage member from an investigation
"""


class InvestigationAPI_members(APIView):
    permission_classes = [permissions.IsAuthenticated, ]
    serializer_class = isa.InvestigationSerializer

    def get_object(self, pk):
        try:
            return models.Investigation.objects.get(pk=pk)
        except models.Investigation.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = isa.InvestigationSerializerTree(snippet)
        return Response(serializer.data["members"])


"""
url:investigation/<int:pk>/tools/

GET: Manage member from an investigation
"""


class InvestigationAPI_tools(APIView):
    permission_classes = [permissions.IsAuthenticated, ]
    serializer_class = isa.InvestigationSerializer

    def get_object(self, pk):
        try:
            return models.Investigation.objects.get(pk=pk)
        except models.Investigation.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = isa.InvestigationSerializerTree(snippet)
        return Response(serializer.data["tools"])


class Investigation_add_study(APIView):
    permission_classes = [permissions.IsAuthenticated, ]
    serializer_class = isa.InvestigationSerializer

    def get_object(self, pk):
        try:
            return models.Investigation.objects.get(pk=pk)
        except models.Investigation.DoesNotExist:
            raise Http404

    def post(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = isa.StudySerializer(data=request.data)
        if serializer.is_valid():
            new_study = serializer.save(author=get_current_user_profile(request.user))
            snippet.studies.add(new_study)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)