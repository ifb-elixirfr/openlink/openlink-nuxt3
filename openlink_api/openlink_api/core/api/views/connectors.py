from openlink_api.core.api.serializers import connectors
from openlink_api.core import models
from openlink_api.core.api.views.api_login import get_current_user_profile
from rest_framework import permissions
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.conf import settings
from django.http import HttpResponse, Http404
from openlink_api.core import connector as tool
from openlink_api.connectors.openlink_galaxy import connectors as galaxy
import os
import hvac

vault_url = (
        settings.AUTH_VAULT_CONFIG["VAULT_HOSTNAME"]
        + settings.AUTH_VAULT_CONFIG["VAULT_PORT"]
)


class Connectors(APIView):
    permission_classes = [permissions.IsAuthenticated, ]

    def get(self, request):
        connector_list = []
        type_allowed = ["mapper", "publisher"]
        if self.request.query_params.get("type"):
            type = str(self.request.query_params.get("type"))
            if type not in type_allowed:
                return Response(status=status.HTTP_400_BAD_REQUEST)
            for connector in tool.get_connectors():
                if connector.get_type() == type:
                    connector_list.append(connector.__name__)
        else:
            for connector in tool.get_connectors():
                connector_list.append(connector.__name__)

        return Response(connector_list)


class toolsListAPI(APIView):
    permission_classes = [permissions.IsAuthenticated, ]
    serializer_class = connectors.ToolSerializer

    def get(self, request):
        """return list of all tools"""
        tools = models.Tool.objects.all()
        tools_serializer = connectors.ToolSerializer(tools, many=True)
        return Response(tools_serializer.data)

    def post(self, request, format=None):
        """create a tools, example body:
                        {
                            "tool": {
                                "name":"galaxytest2",
                                "connector":"GalaxyConnector"
                            },
                            "toolparam": {
                                "url":"http://127.0.0.1:8080/",
                                "login": "test",
                                "password": "testpassword"
                            },
                            "async": "off"
                        }
                    """
        try:
            tool_data = request.data["tool"]
            tool_param_data = request.data["toolparam"]
            async_param = request.data["async"]
            connector_class = tool.get_connector_class(tool_data["connector"])
            connector_class.test_connection(tool_param_data["url"], tool_param_data["login"], tool_param_data["password"])
        except:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        serializer_tool = connectors.ToolSerializer(data=tool_data)
        if serializer_tool.is_valid():
            new_tool = serializer_tool.save(author=request.user)

            public_param = {k: tool_param_data[k] for k in connector_class.list_public_param() if k in tool_param_data}
            private_param = {k: tool_param_data[k] for k in connector_class.list_private_param() if k in tool_param_data}

            for key, value in public_param.items():
                serializer_toolparam = connectors.ToolparamSerializer(data={"key": key, "value": value})
                if serializer_toolparam.is_valid():
                    serializer_toolparam.save(tool_id=new_tool)
                else:
                    return Response(serializer_toolparam.errors, status=status.HTTP_400_BAD_REQUEST)

            serializer_toolstate = connectors.Tool_state_userSerializer(
                data={"asyncTask": async_param})
            serializer_toolstate.is_valid()
            new_tool_state = serializer_toolstate.save(profile=get_current_user_profile(request.user), tool=new_tool)

            write_secret_in_vault(request, new_tool.id, private_param, shared=new_tool_state.get_asynctask_state())



        else:
            return Response(serializer_tool.errors, status=status.HTTP_400_BAD_REQUEST)

        return Response({"Tool": serializer_tool.data, "Toolparam": serializer_toolparam.data},
                        status=status.HTTP_201_CREATED)


class toolsAPI(APIView):
    permission_classes = [permissions.IsAuthenticated, ]
    serializer_class = connectors.ToolSerializer

    def get_object(self, pk):
        try:
            return models.Tool.objects.get(pk=pk)
        except models.Tool.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        """return specified tool data"""
        snippet = self.get_object(pk)
        serializer = connectors.ToolSerializer(snippet)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        """modify specified tool"""
        snippet = self.get_object(pk)
        serializer = connectors.ToolSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        """delete specified tool"""
        snippet = self.get_object(pk)
        tool_state = models.ToolStateProfile.objects.get(tool=snippet, profile=get_current_user_profile(request.user))
        delete_secret_in_vault(request, snippet.id, tool_state.get_asynctask_state())
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


def write_secret_in_vault(request, tool_id, data, shared):
    """register secret in vault"""
    vault_client = hvac.Client(
        url=vault_url, token=request.session["vault_token"]
    )
    reponse = vault_client.lookup_token()
    if shared:
        path = os.path.join(
            "shared", reponse["data"]["entity_id"], str(tool_id)
        )
    else:
        path = os.path.join(reponse["data"]["entity_id"], str(tool_id))
    vault_client.secrets.kv.v1.create_or_update_secret(
        mount_point="openlink", path=path, secret=data
    )


def delete_secret_in_vault(request, tool_id, shared):
    """delete secret in vault"""
    vault_client = hvac.Client(
        url=vault_url, token=request.session["vault_token"]
    )
    reponse = vault_client.lookup_token()
    if shared:
        path = os.path.join(
            "shared", reponse["data"]["entity_id"], str(tool_id)
        )
    else:
        path = os.path.join(reponse["data"]["entity_id"], str(tool_id))
    vault_client.secrets.kv.v1.delete_secret(mount_point="openlink", path=path)


# class ToolLinkAPI(APIView):
#     permission_classes = [permissions.IsAuthenticated, ]
#     serializer_class = connectors.ToolSerializer
#
#     def post(self, request, pk, format=None):
#         """link yourself to an already existing tool specified by it's id
#         {
#             "login": "test",
#             "password": "testpassword"
#         }
#         """
#         try:
#             tool = models.Tool.objects.get(pk=pk)
#             connector_class = tool.get_connector_class(tool.connector)
#             test_connector_connection(connector_class, request.data, tool.get_public_param("url"))
#         except:
#             return Response(status=status.HTTP_400_BAD_REQUEST)
#
#         tool.user_list.add(get_current_user_profile(request.user))
#         write_secret_in_vault(request, tool.id, False)
#
#         return Response(status=status.HTTP_204_NO_CONTENT)
#
#     def delete(self, request, pk, format=None):
#         """delete user credentials in the vault for a specified tool"""
#         tool = models.Tool.objects.get(pk=pk, connector="GalaxyConnector")
#         delete_secret_in_vault(request, tool.id)
#         return Response(status=status.HTTP_204_NO_CONTENT)
