import hvac

from django.http import HttpResponse
from django.conf import settings
from django.contrib.auth.signals import user_logged_out
from rest_framework.renderers import JSONRenderer

from openlink_api.core.api.serializers.base import LoginUserSerializer, UserSerializer
from openlink_api.core.models import Profile
from rest_framework import status
from rest_framework import generics, permissions
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated

from knox.auth import TokenAuthentication
from knox.models import AuthToken

vault_url = settings.AUTH_VAULT_CONFIG["VAULT_HOSTNAME"] + settings.AUTH_VAULT_CONFIG["VAULT_PORT"]


""" List of supported http method
"get"
"post"
"put"
"patch"
"delete"
"head"
"options"
"trace"
"""


class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)


class UserAPI(generics.RetrieveAPIView):
    permission_classes = [permissions.IsAuthenticated, ]
    serializer_class = UserSerializer

    def get_object(self):
        return self.request.user


class LogoutAPI(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, format=None):
        if "vault_token" in request.session:
            try:
                vault_client = hvac.Client(url=vault_url, token=request.session['vault_token'])
                vault_client.logout(revoke_token=True)
                print("vault token revoked")
            except Exception as e:
                print("couldn't revoke vault  token")
                print(e)

        request.session.flush()
        request._auth.delete()
        user_logged_out.send(sender=request.user.__class__,
                             request=request, user=request.user)
        return Response(None, status=status.HTTP_204_NO_CONTENT)

"""
Ldap login view also authenticate on vault server to get token and store it in server side session.
"""
class LoginAPI_LDAP(generics.GenericAPIView):
    serializer_class = LoginUserSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data
        if user:
            if user.is_active:
                # check vault login
                try:
                    client = hvac.Client(url=vault_url)
                    response = client.auth.ldap.login(
                        username=request.data["username"],
                        password=request.data["password"],
                        mount_point='ldap'
                    )
                    vault_token = response["auth"]["client_token"]
                    request.session["vault_token"] = vault_token
                    profile = get_current_user_profile(user)
                    profile.set_vaultid(response["auth"]["entity_id"])
                    profile.save()
                except:
                    return Response({"Vault Error": "could not connect to vault with given credentials"})
        return Response({
            "user": UserSerializer(user, context=self.get_serializer_context()).data,
            "token": AuthToken.objects.create(user)[1]
        })


# Return profile from user instance
def get_current_user_profile(user):
    return Profile.objects.get(user=user)
