from guardian.shortcuts import assign_perm
import json
from openlink_api.core.api.serializers import isa, connectors, base
from openlink_api.core import models
from openlink_api.core.api.views.api_login import get_current_user_profile
from rest_framework import permissions
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.shortcuts import get_object_or_404
from django.http import HttpResponse, Http404
from openlink_api.core import connector as conn


class IsaObjectAPI(APIView):
    permission_classes = [permissions.IsAuthenticated, ]
    serializer_class = isa.IsaObjectSerializer

    def get_object(self, pk):
        try:
            return models.ISAObject.objects.get(pk=pk)
        except models.ISAObject.DoesNotExist:
            raise Http404

    def get(self, request, *args, **kwargs):
        snippet = self.get_object(kwargs["isaobject_id"])
        try:
            isa_element = snippet.get_isa_class().get_investigation(id=kwargs["isaobject_id"])
        except:
            return Response({"Failure": "Error", "Error": "Given isaobject is not link to any investigation"}, status=status.HTTP_400_BAD_REQUEST)
        tool_list = []
        for tool in isa_element.list_tools():
            if tool.is_user_linked(get_current_user_profile(request.user)):
                tool_list.append(connectors.ToolSerializer(tool).data)
        return Response(tool_list)


class IsaObject_MappingAPI(APIView):
    permission_classes = [permissions.IsAuthenticated, ]
    serializer_class = isa.IsaObjectSerializer

    def get_object(self, pk):
        try:
            return models.ISAObject.objects.get(pk=pk)
        except models.ISAObject.DoesNotExist:
            raise Http404

    def get(self, request, *args, **kwargs):
        snippet = self.get_object(kwargs["isaobject_id"])
        tool = models.Tool.objects.get(id=kwargs['tool_id'])
        tool_state_profile = models.ToolStateProfile.objects.get(profile=get_current_user_profile(request.user), tool=tool)

        if tool_state_profile.get_asynctask_state():
            connector = conn.get_connector(tool, request.session["vault_token"], get_current_user_profile(request.user).get_vaultid())
        else:
            connector = conn.get_connector(tool, request.session["vault_token"])

        mapping_object_list = []
        for data in connector.get_data_root():
            serializer = base.ContainerDataObjectSerializer(data)
            mapping_object_list.append(serializer.data)
        return Response(mapping_object_list)


class IsaObject_Mapping_childAPI(APIView):
    permission_classes = [permissions.IsAuthenticated, ]
    serializer_class = isa.IsaObjectSerializer

    def get_object(self, pk):
        try:
            return models.ISAObject.objects.get(pk=pk)
        except models.ISAObject.DoesNotExist:
            raise Http404

    def get(self, request, *args, **kwargs):
        snippet = self.get_object(kwargs["isaobject_id"])
        tool = models.Tool.objects.get(id=kwargs['tool_id'])
        tool_state_profile = models.ToolStateProfile.objects.get(profile=get_current_user_profile(request.user),
                                                                 tool=tool)

        if tool_state_profile.get_asynctask_state():
            connector = conn.get_connector(tool, request.session["vault_token"],
                                           get_current_user_profile(request.user).get_vaultid())
        else:
            connector = conn.get_connector(tool, request.session["vault_token"])

        mapping_object_list = []
        for data in connector.get_child_data(kwargs["container_id"]):
            serializer = base.ContainerDataObjectSerializer(data)
            mapping_object_list.append(serializer.data)
        return Response(mapping_object_list)


class IsaObject_DatalinkAPI(APIView):
    permission_classes = [permissions.IsAuthenticated, ]
    serializer_class = isa.IsaObjectSerializer

    def get_object(self, pk):
        try:
            return models.ISAObject.objects.get(pk=pk)
        except models.ISAObject.DoesNotExist:
            raise Http404

    def get(self, request, *args, **kwargs):
        snippet = self.get_object(kwargs["isaobject_id"])
        datalink_list = []
        for datalink in snippet.datalink.all():
            serializer = isa.DatalinkSerializer(datalink)
            datalink_list.append(serializer.data)
        return Response(datalink_list)

    def post(self,  request,  *args, **kwargs):
        snippet = self.get_object(kwargs["isaobject_id"])
        data = request.data
        data["tool_id"] = kwargs["tool_id"]
        serializer = isa.DatalinkSerializer(data=data)
        if serializer.is_valid():
            datalink = serializer.save(author=get_current_user_profile(request.user))
            snippet.datalink.add(datalink)
        return Response(serializer.data, status=status.HTTP_201_CREATED)
