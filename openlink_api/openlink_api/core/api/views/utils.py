from openlink_api.core import models
from rest_framework import permissions
from rest_framework.views import APIView
from rest_framework.response import Response
from openlink_api.core.api.serializers import base
from django.http import Http404



class List_all_ProfileAPI(APIView):
    permission_classes = [permissions.IsAuthenticated, ]
    serializer_class = base.ProfileSerializer

    def get(self, request):
        profiles = models.Profile.objects.all()
        profiles_serialized = base.ProfileSerializer(profiles, many=True)
        return Response(profiles_serialized.data)


class ProfileAPI(APIView):
    permission_classes = [permissions.IsAuthenticated, ]
    serializer_class = base.ProfileSerializer

    def get_object(self, pk):
        try:
            return models.Profile.objects.get(pk=pk)
        except models.Profile.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = base.ProfileSerializer(snippet)
        return Response(serializer.data)