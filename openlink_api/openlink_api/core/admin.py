from django.contrib import admin

# from django.contrib.admin.options import get_content_type_for_model
# from django.urls import reverse
# from django.utils.html import format_html
# from django.utils.translation import ugettext
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User

from openlink_api.core.models import (
    Profile,
    Member,
    Tool,
    Toolparam,
    ToolStateProfile,
    Publication,
    DataLink,
    Assay,
    Study,
    Investigation,
    Project
)


# Define an inline admin descriptor for Profile model
# which acts a bit like a singleton
class ProfileInline(admin.StackedInline):
    model = Profile
    can_delete = False
    verbose_name_plural = "profile"


# Define a new User admin
class UserAdmin(BaseUserAdmin):
    inlines = (ProfileInline,)


# Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)


class ViewOnSiteModelAdmin(admin.ModelAdmin):
    class Media:
        css = {
            "all": (
                "https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css",
            )
        }


class ToolAdmin(admin.ModelAdmin):
    list_display = ("name", "id")


class InvestigationAdmin(admin.ModelAdmin):
    list_display = ["name", "id"]


class StudyAdmin(admin.ModelAdmin):
    list_display = ["name", "id"]


class AssayAdmin(admin.ModelAdmin):
    list_display = ["name", "id"]


class DataLinkAdmin(admin.ModelAdmin):
    list_display = ["name", "id"]


class MemberAdmin(admin.ModelAdmin):
    list_display = ["id", "profile", "permission"]


admin.site.register(Investigation, InvestigationAdmin)
admin.site.register(Study, StudyAdmin)
admin.site.register(Assay, AssayAdmin)
admin.site.register(Tool, ToolAdmin)
admin.site.register(Toolparam)
admin.site.register(Profile)
admin.site.register(Member, MemberAdmin)
admin.site.register(Project)
admin.site.register(ToolStateProfile)
admin.site.register(Publication)
admin.site.register(DataLink)
