from django.apps import AppConfig


class OpenlinkAppConfig(AppConfig):
    name = "openlink_api.core"
