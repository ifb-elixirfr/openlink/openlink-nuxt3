import logging
import os

import hvac
from ckeditor.fields import RichTextField
from django.conf import settings
from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

logger = logging.getLogger(__name__)

vault_url = (
    settings.AUTH_VAULT_CONFIG["VAULT_HOSTNAME"]
    + settings.AUTH_VAULT_CONFIG["VAULT_PORT"]
)


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    vault_id = models.CharField(max_length=100, blank=True)
    email = models.CharField(max_length=100, blank=True)

    def set_vaultid(self, id):
        self.vault_id = id
        return self

    def get_vaultid(self):
        return self.vault_id

    def __str__(self):
        return str(self.user)


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()


class Member(models.Model):
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
    CHOICES = (
        ('admin', 'Administrator'),
        ('contributor', 'Contributor'),
        ('collaborator', 'Collaborator'),

    )
    permission = models.CharField(max_length=12, choices=CHOICES)

    def __str__(self):
        return str(self.profile) + ": " + str(self.permission)


class Tool(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    connector = models.CharField(max_length=100, blank=True, null=True)
    author = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)
    date_created = models.DateTimeField(auto_now_add=True, blank=True, null=True)

    def get_public_param(self, key):
        value = Toolparam.objects.values_list("value", flat=True).get(
            key=key, tool_id_id=str(self.id)
        )
        return value

    def get_private_param(self, key, token, user_vault_id=None):
        if token is not None:
            try:
                vault_client = hvac.Client(url=vault_url, token=token)
                vault_client.kv.default_kv_version = 1
                reponse = vault_client.lookup_token()
                if user_vault_id:
                    path = os.path.join("shared", user_vault_id, str(self.id))
                else:
                    path = os.path.join(reponse["data"]["entity_id"], str(self.id))
                value = vault_client.secrets.kv.v1.read_secret(
                    mount_point="openlink", path=path
                )
                return value["data"][key]
            except Exception as e:
                raise e
        else:
            return None

    def is_user_linked(self, profile):
        try:
            ToolStateProfile.objects.get(profile=profile, tool=self)
            return True
        except:
            return False


class Toolparam(models.Model):
    tool_id = models.ForeignKey(Tool, on_delete=models.CASCADE, null=True)
    key = models.CharField(max_length=100, blank=True, null=True)
    value = models.CharField(max_length=100, blank=True, null=True)


class ToolStateProfile(models.Model):
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE, null=True)
    tool = models.ForeignKey(Tool, on_delete=models.CASCADE, null=True)
    CHOICES = (
        ('on', 'Asynchronous Task allowed'),
        ('off', 'Asynchronous Task not allowed'),
    )
    asyncTask = models.CharField(max_length=3, choices=CHOICES)

    def get_asynctask_state(self):
        if self.asyncTask == "on":
            return True
        else:
            return False


class Publication(models.Model):
    doi = models.CharField(max_length=100)
    version = models.CharField(max_length=3)
    url = models.CharField(max_length=1000)
    date_created = models.DateTimeField(blank=True, null=True)


class DataLink(models.Model):
    name = models.CharField(max_length=1000, blank=True)
    tool_id = models.ForeignKey(Tool, on_delete=models.CASCADE, blank=True, null=True)
    target_ressource = models.CharField(max_length=1000, blank=True, null=True)
    target_url = models.CharField(max_length=1000, blank=True, null=True)
    size = models.BigIntegerField(blank=True, null=True)
    author = models.ForeignKey(Profile, on_delete=models.CASCADE, null=True)
    publication = models.ManyToManyField(Publication, blank=True)


class ISAObject(models.Model):
    name = models.CharField(max_length=1000)
    description = RichTextField(blank=True, null=True, config_name="zenodo")
    author = models.ForeignKey(Profile, on_delete=models.CASCADE, null=True)
    date_created = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    date_modified = models.DateTimeField(blank=True, null=True)
    datalink = models.ManyToManyField(DataLink, blank=True)
    ISA_TYPE = (
        ('Assay', 'Assay'),
        ('Study', 'Study'),
        ('Investigation', 'Investigation'),
    )
    type = models.CharField(max_length=100, null=True, choices=ISA_TYPE)

    def get_isa_class(self):
        if self.type == "Assay":
            return Assay
        if self.type == "Study":
            return Study
        if self.type == "Investigation":
            return Investigation

    def list_datalink(self):
        return self.datalink.all()


class Assay(ISAObject):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.type = "Assay"

    def __str__(self):
        return self.name

    def get_investigation(id):
        id = id.id if type(id) == Assay else id
        return Investigation.objects.get(studies__assays__id=id)

    def get_study(id):
        id = id.id if type(id) == Assay else id
        return Study.objects.get(assays__id=id)


class Study(ISAObject):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.type = "Study"
    assays = models.ManyToManyField(Assay, blank=True)

    def get_investigation(id):
        id = id.id if type(id) == Study else id
        return Investigation.objects.get(studies__id=id)

    def get_assay(id):
        return Assay.objects.filter(study__id=id)


class Investigation(ISAObject):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.type = "Investigation"
    studies = models.ManyToManyField(Study, blank=True)
    members = models.ManyToManyField(Member, blank=True)
    tools = models.ManyToManyField(Tool, blank=True)

    def list_studies(self):
        return self.studies.all()

    def list_members(self):
        return self.members.all()

    def list_tools(self):
        return self.tools.all()

    def get_investigation(id):
        return Investigation.objects.get(id=id)


class Project(models.Model):
    name = models.CharField(max_length=100)
    description = RichTextField(blank=True, null=True)
    investigation = models.ManyToManyField(Investigation, blank=True)
    member = models.ManyToManyField(Member, blank=True)
