from django.apps import AppConfig


class OpenlinkSeafileConfig(AppConfig):
    name = "openlink_api.connectors.openlink_seafile"
