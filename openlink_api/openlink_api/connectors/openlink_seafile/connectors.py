import io
import logging
import time
import zipfile
import openlink_api.core.connector
import requests
from django import forms
from openlink_api.core.connector import (
    AuthentificationError,
    ContainerDataObject,
    ContainerObject,
    DataObject,
    Mapper,
    ToolConnector,
    ToolForm,
    ToolUnreachableError,
    defaultError,
    permissionError,
)

logger = logging.getLogger(__name__)


class SeafileConnector(Mapper):
    def __init__(self, tool, token, user_vault_id=None):
        self.url = tool.get_public_param("url")
        self.token = tool.get_private_param("token", token, user_vault_id=user_vault_id)
        self.tool = tool

    @classmethod
    def get_name(cls):
        return "Seafile"

    @classmethod
    def get_type(cls):
        return "Mapper"

    @classmethod
    def get_data_structure(cls):
        return openlink_api.core.connector.TREE_STRUCTURE

    @classmethod
    def has_access_url(cls):
        return True

    @classmethod
    def get_logo(cls):
        logo = "images/logo-seafile.png"
        return logo

    @classmethod
    def get_color(cls):
        color = "#F1D583"
        return color

    @classmethod
    def has_mapping_options(cls):
        return False

    @classmethod
    def check_status_code(cls, code):
        code = code
        logger.debug(code)
        if code != requests.codes.ok:
            if code == 400:
                raise AuthentificationError(cls, "login or password")
            elif code == (500 or 429):
                raise ToolUnreachableError(cls)
            elif code == 403:
                raise permissionError(cls)
            else:
                raise defaultError(cls)

    @classmethod
    def get_api_token(cls, url, login, password):
        info = {"username": login, "password": password}
        r = requests.post(url, data=info)
        cls.check_status_code(r.status_code)
        token = r.json()["token"]
        return token

    def get_api_url(self):
        url_api = "%s/api/v2.1/" % (self.url)
        url_api = url_api.replace("//api", "/api")
        return url_api

    def get_api_url2(self):
        url_api = "%s/api2/" % (self.url)
        url_api = url_api.replace("//api", "/api")
        return url_api

    def get_url_root(self):
        url_root = "%s/seafhttp/" % (self.url)
        url_root = url_root.replace("//seafhttp", "/seafhttp")
        return url_root

    def get_url_link_to_an_object(self, obj_type, obj_id):
        # return a url to display for current user
        WEB_DISPLAY = "/library/"
        if obj_type == "assay":
            url = self.url + WEB_DISPLAY + obj_id
        elif obj_type == "data":
            if "/file/" in obj_id:
                WEB_DISPLAY = "/lib/"
            url = self.url + WEB_DISPLAY + obj_id
        else:
            url = self.url + WEB_DISPLAY + obj_id
        url = url.replace("//library", "/library").replace("//lib", "/lib")
        return url

    def get_space_info(self, objects_id, mapping_object=None):
        if mapping_object:
            jobid = utils.start_async_task()
        try:
            size_str = 0
            type_d = ""
            for object_path in objects_id:
                if "/" in object_path:
                    project = object_path.split("/")[0]
                    name = object_path.split("/")[-1]
                    path_id = object_path.split("/", 2)[2].split("/")[:-1]
                    path_dir = "/".join(path_id)
                    if object_path.split("/")[1] == "file":
                        type_d = "file"
                        url = "{url_api}repos/{id}/dir/?p=/{dir_id}".format(
                            url_api=self.get_api_url(), id=project, dir_id=path_dir
                        )
                    else:
                        type_d = "dir"
                        url = "{url_api}repos/{id}/dir/?t=f&recursive=1&p=/{dir_id}".format(
                            url_api=self.get_api_url(), id=project, dir_id=path_dir
                        )
                else:
                    url = "{url_api}repos/{id}/".format(
                        url_api=self.get_api_url(), id=object_path
                    )
                headers = {"Authorization": "Token " + str(self.token)}
                try:
                    response = requests.get(url, headers=headers)
                    logger.debug(SeafileConnector.check_status_code(response.status_code))
                except Exception:
                    size_str = -1
                    size = int(size_str)
                    return size
                elements = response.json()
                for element in elements["dirent_list"]:
                    if type_d == "file":
                        if element["type"] == "file" and element["name"] == name:
                            size_str += element["size"]
                    elif type_d == "dir":
                        if element["type"] == "file":
                            size_str += element["size"]
            size = int(size_str)
            if mapping_object:
                mapping_object.size = size
                mapping_object.save()
                utils.finish_async_task(jobid, size)
            else:
                return size
        except Exception as e:
            utils.error_async_task(jobid)
            return e


    def download(self, object_id, path):
        headers = {"Authorization": "Token " + self.token}
        if "/file" in object_id:
            object_id_m = object_id.split("/")[0]
            directory_id = object_id.rsplit("%s/file/" % object_id_m)[1]
            url = "{url_api}repos/{id}/file/?p=/{file}".format(
                url_api=self.get_api_url2(), id=object_id_m, file=directory_id
            )

            try:
                response = requests.get(url, headers=headers)
                SeafileConnector.check_status_code(response.status_code)
            except Exception as e:
                raise e
            elements = response.json()
            try:
                r = requests.get(elements, headers=headers)
                SeafileConnector.check_status_code(r.status_code)
            except Exception as e:
                raise e
            open("%s/%s" % (path, object_id.split("/")[-1]), "wb").write(r.content)
        else:
            object_id_m = object_id.split("/")[0]
            directory_id = object_id.rsplit("%s/" % object_id_m)[1]
            parent_dir = directory_id.rsplit("/", 1)[0]
            parent_dir = parent_dir.split("/", 1)[1]
            dir = object_id.split("/")[-1]
            url = "{url_api}repos/{id}/zip-task/?parent_dir={parent}&dirents={dir}".format(
                url_api=self.get_api_url(), id=object_id_m, parent=parent_dir, dir=dir
            )
            try:
                response = requests.get(url, headers=headers)
                SeafileConnector.check_status_code(response.status_code)
            except Exception as e:
                logger.debug(e)
                raise e
            logger.debug(response.text)
            elementsdict = response.json()
            ziptoken = elementsdict["zip_token"]
            url2 = "{url_api}query-zip-progress/?token={token}".format(
                url_api=self.get_api_url(), token=ziptoken
            )
            download = False
            while not download:
                try:
                    response = requests.get(url2, headers=headers)
                    SeafileConnector.check_status_code(response.status_code)
                except Exception as e:
                    raise e
                elementsdict = response.json()
                if elementsdict["zipped"] == elementsdict["total"]:
                    url3 = "{url_root}zip/{token}".format(
                        url_root=self.get_url_root(), token=ziptoken
                    )
                    try:
                        r = requests.get(url3, headers=headers)
                        SeafileConnector.check_status_code(r.status_code)
                    except Exception as e:
                        raise e
                    z = zipfile.ZipFile(io.BytesIO(r.content))
                    z.extractall(path)
                    download = True
                else:
                    time.sleep(0.125)

    def check_file_access(self, object_id):
        headers = {"Authorization": "Token " + self.token}
        if "/file" in object_id:
            object_id_m = object_id.split("/")[0]
            directory_id = object_id.rsplit("%s/file/" % object_id_m)[1]
            url = "{url_api}repos/{id}/file/?p=/{file}".format(
                url_api=self.get_api_url2(), id=object_id_m, file=directory_id
            )
            try:
                response = requests.get(url, headers=headers)
                SeafileConnector.check_status_code(response.status_code)
            except Exception as e:
                logger.debug(e)
            elements = response.json()
            try:
                r = requests.get(elements, headers=headers)
                SeafileConnector.check_status_code(r.status_code)
                access = True
            except Exception as e:
                access = False
                logger.debug(e)
        else:
            object_id_m = object_id.split("/")[0]
            directory_id = object_id.rsplit("%s/" % object_id_m)[1]
            parent_dir = directory_id.rsplit("/", 1)[0]
            parent_dir = parent_dir.split("/", 1)[1]
            dir = object_id.split("/")[-1]
            url = "{url_api}repos/{id}/zip-task/?parent_dir={parent}&dirents={dir}".format(
                url_api=self.get_api_url(), id=object_id_m, parent=parent_dir, dir=dir
            )
            try:
                response = requests.get(url, headers=headers)
                SeafileConnector.check_status_code(response.status_code)
                access = True
            except Exception as e:
                access = False
                logger.debug(e)
        return access

    def get_description(self):
        return ""

    def get_information(self, type, id):
        headers = {"Authorization": "Token " + self.token}
        info = {}
        url2 = None
        if "/file/" in id:
            project_id_m = id.split("/")[0]
            directory_id = id.rsplit("%s/file/" % project_id_m)[1]
            url = "{url_api}repos/{id}/file/detail/?p=/{file}".format(
                url_api=self.get_api_url2(), id=project_id_m, file=directory_id
            )
        elif "/" in id:
            project_id_m = id.split("/")[0]
            directory_id = id.split("/", 2)[2]
            url = "{url_api}repos/{id}/dir/detail/?path={path}".format(
                url_api=self.get_api_url(), id=project_id_m, path=directory_id
            )
            url2 = "{url_api}repos/{id}/dir/shared_items/?p=/&share_type=".format(
                url_api=self.get_api_url2(), id=project_id_m
            )
        else:
            url = "{url_api}repos/{id}".format(url_api=self.get_api_url2(), id=id)
            url2 = "{url_api}repos/{id}/dir/shared_items/?p=/&share_type=".format(
                url_api=self.get_api_url2(), id=id
            )
        try:
            response = requests.get(url, headers=headers)
            SeafileConnector.check_status_code(response.status_code)
        except Exception as e:
            raise e
        elementsdict = response.json()
        info["title"] = elementsdict["name"]
        if url2 is not None:
            url2 += "user"
            response = requests.get(url2, headers=headers)
            elementsdict = response.json()

            info["user"] = []
            for element in elementsdict:
                info["user"].append(element["user_info"]["nickname"])

            url2 = url2.replace("user", "group")
            response = requests.get(url2, headers=headers)
            elementsdict = response.json()
            for element in elementsdict:
                groupurl = "{url_api}groups/{id}/members".format(
                    url_api=self.get_api_url(), id=element["group_info"]["id"]
                )
                response = requests.get(groupurl, headers=headers)
                elementsdict = response.json()
                for groupuser in elementsdict:
                    info["user"].append(groupuser["name"])
        return info


ToolConnector.register(SeafileConnector)
