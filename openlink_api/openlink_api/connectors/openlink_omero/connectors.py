# from django.shortcuts import render
import logging
import os
from time import sleep
from urllib.parse import urlparse
import omero
import openlink_api.core.connector
from django import forms
from Glacier2 import PermissionDeniedException
from Ice import DNSException
from omero.cli import cli_login
from omero.gateway import BlitzGateway
from openlink_api.core.connector import (
    AuthentificationError,
    ContainerDataObject,
    DataObject,
    Mapper,
    ToolConnector,
    ToolForm,
    ToolUnreachableError,
    defaultError,
)

logger = logging.getLogger(__name__)


class OmeroConnector(Mapper):
    def __init__(self, tool, token, user_vault_id=None):
        self.url = tool.get_public_param("url")
        self.login = tool.get_private_param("login", token, user_vault_id)
        self.password = tool.get_private_param("password", token, user_vault_id)
        self.tool = tool

    @classmethod
    def get_name(cls):
        return "Omero"

    @classmethod
    def get_type(cls):
        return "Mapper"

    @classmethod
    def get_data_structure(cls):
        return openlink_api.core.connector.LIST_STRUCTURE

    @classmethod
    def has_access_url(cls):
        return True

    @classmethod
    def get_logo(cls):
        logo = "images/logo-omero.svg"
        return logo

    @classmethod
    def get_color(cls):
        color = "#7CDD98"
        return color

    @classmethod
    def has_mapping_options(cls):
        return False

    @classmethod
    def get_port(cls):
        return 4064

    @classmethod
    def check_status_code(cls, code):
        if type(code) == PermissionDeniedException:
            raise AuthentificationError(cls, "login or password")
        elif type(code) == DNSException:
            raise ToolUnreachableError(cls)
        elif type(code) == omero.ClientError:
            raise ToolUnreachableError(cls)
        else:
            raise defaultError(cls)

    def get_host(self):
        url_parse = urlparse(self.url)
        host = url_parse.hostname
        return host

    def get_api_url(self):
        WEB_HOST = "v0/"
        url_api = "%s/api/%s" % (self.url, WEB_HOST)
        url_api = url_api.replace("//api", "/api")
        return url_api


    def get_space_info(self, objects_id, mapping_object=None):
        import omero
        if mapping_object:
            jobid = utils.start_async_task()
        try:
            client = omero.client(self.get_host(), self.get_port())
            session = client.createSession(str(self.login), str(self.password))
        except PermissionDeniedException:
            size_str = "invalid credentials"
            space = size_str
            utils.error_async_task(jobid)
            return size_str
        except omero.ClientError:
            size_str = "Error"
            space = size_str
            utils.error_async_task(jobid)
            return size_str
        except DNSException:
            size_str = "Error"
            space = size_str
            utils.error_async_task(jobid)
            return size_str
        from omero.cmd import DiskUsage2
        try:
            req = DiskUsage2()
            objects = {}
            classes = set()
            ids = [int(id) for id in objects_id]
            objects.setdefault("Image", []).extend(ids)
            import omero.callbacks

            req.targetObjects, req.targetClasses = (objects, list(classes))
            handle = session.submit(req)
            # sleep(1)
            cb = omero.callbacks.CmdCallbackI(client, handle)
            sleep(0.5)
            rsp = cb.getResponse()
            max_loop = 8
            loop = 0
            while rsp is None and loop <= max_loop:
                rsp = cb.getResponse()
                sleep(0.5)
                loop += 1
            from omero.util.text import filesizeformat

            if rsp is not None:
                size = sum(rsp.totalBytesUsed.values())
                size_str = filesizeformat(size)
            else:
                size = 0
                size_str = "0 KB"

            space = size
            if mapping_object:
                mapping_object.size = space
                mapping_object.save()
                utils.finish_async_task(jobid, space)
            else:
                return space
        except Exception as e:
            utils.error_async_task(jobid)
            return e


    def download(self, object_id, target_dir):
        with cli_login(
            "%s@%s:%s" % (str(self.login), str(self.get_host()), str(self.get_port())),
            "-w",
            str(self.password),
        ) as cli:
            conn = BlitzGateway(client_obj=cli._client)
            conn.SERVICE_OPTS.setOmeroGroup("-1")
            # parent = conn.getObject("Dataset", object_id)
            # data_dir = os.path.join(target_dir, parent.name)
            os.makedirs(target_dir, exist_ok=True)
            image = conn.getObject("Image", object_id)
            # image_dir = os.path.join(target_dir, image.name)
            cli.invoke(["download", f"Image:{image.getId()}", target_dir])
            # for image in parent.listChildren():
            #     image_dir = os.path.join(data_dir, image.name)
            #     cli.invoke(["download", f"Image:{image.id}", image_dir])
        return None

    def check_file_access(self, object_id):
        with cli_login(
            "%s@%s:%s" % (str(self.login), str(self.get_host()), str(self.get_port())),
            "-w",
            str(self.password),
        ) as cli:
            conn = BlitzGateway(client_obj=cli._client)
            conn.SERVICE_OPTS.setOmeroGroup("-1")
            try:
                conn.getObject("Image", object_id)
                access = True
            except Exception as e:
                logging.debug(e)
                access = False
        return access

    def get_information(self, type, id):
        return {}


ToolConnector.register(OmeroConnector)
