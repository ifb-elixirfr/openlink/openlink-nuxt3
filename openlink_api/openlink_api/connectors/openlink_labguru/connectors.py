# from django.shortcuts import render
import logging
import os
import openlink_api.core.connector
import requests
from django import forms
from django.dispatch import receiver
from openlink_api.core.connector import (
    AuthentificationError,
    ContainerDataObject,
    DataObject,
    Mapper,
    ToolConnector,
    ToolForm,
    defaultError,
)

from openlink_api.core.models import ISAObject, Profile, Tool

logger = logging.getLogger(__name__)



class LabGuruConnector(Mapper):
    def __init__(self, tool, token, user_vault_id=None):
        self.url = tool.get_public_param("url")
        self.token = tool.get_private_param("token", token, user_vault_id)
        self.tool = tool

    @classmethod
    def get_name(cls):
        return "LabGuru"

    @classmethod
    def get_type(cls):
        return "Mapper"

    @classmethod
    def get_data_structure(cls):
        return openlink_api.core.connector.LIST_STRUCTURE

    @classmethod
    def has_access_url(cls):
        return True

    @classmethod
    def get_logo(cls):
        logo = "images/logo-labguru.webp"
        return logo

    @classmethod
    def get_color(cls):
        color = "#CEA4F7"
        return color

    @classmethod
    def check_status_code(cls, r):
        if r != requests.codes.ok:
            if r == 401:
                raise AuthentificationError(cls, "token")
            else:
                raise defaultError(cls)

    @classmethod
    def test_private_token(cls, url, private_token):
        full_url = url + private_token
        r = requests.get(full_url)
        cls.check_status_code(r.status_code)

    def get_api_url(self):
        WEB_HOST = "v1/"
        url_api = "%s/api/%s" % (self.url, WEB_HOST)
        url_api = url_api.replace("//api", "/api")
        return url_api


    def get_space_info(self, objects_id):
        space = "None"
        return space

ToolConnector.register(LabGuruConnector)
