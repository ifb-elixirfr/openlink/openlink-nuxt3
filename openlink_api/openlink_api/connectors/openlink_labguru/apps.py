from django.apps import AppConfig


class OpenlinkLabguruConfig(AppConfig):
    name = "openlink_api.connectors.openlink_labguru"
