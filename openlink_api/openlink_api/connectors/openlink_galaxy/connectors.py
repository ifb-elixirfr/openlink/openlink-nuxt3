import logging
import os
import openlink_api.core.connector
import requests
from bioblend import galaxy
from django import forms
from openlink_api.core.connector import (
    AuthentificationError,
    ContainerDataObject,
    Mapper,
    ToolConnector,
    ToolForm,
    defaultError,
)

logger = logging.getLogger(__name__)


class GalaxyConnector(Mapper):
    def __init__(self, tool, token, user_vault_id=None):
        self.url = tool.get_public_param("url")
        self.login = tool.get_private_param("login", token, user_vault_id)
        self.password = tool.get_private_param("password", token, user_vault_id)
        self.tool = tool

    #
    # Default fonction
    #

    @classmethod
    def get_name(cls):
        return "Galaxy"

    @classmethod
    def get_type(cls):
        return "mapper"

    @classmethod
    def get_form(cls):
        return {"url": "", "login": "", "password": ""}

    @classmethod
    def list_public_param(cls):
        return ["url"]

    @classmethod
    def list_private_param(cls):
        return ["login", "password"]

    @classmethod
    def get_data_structure(cls):
        return openlink_api.core.connector.LIST_STRUCTURE

    @classmethod
    def has_access_url(cls):
        return True

    @classmethod
    def get_logo(cls):
        logo = "images/logo-galaxy.png"
        return logo

    @classmethod
    def get_color(cls):
        color = "#7FAEE5"
        return color

    @classmethod
    def check_status_code(cls, r):
        if r != requests.codes.ok:
            if r == 401:
                raise AuthentificationError(cls, "email or password")
            else:
                raise defaultError(cls)

    @classmethod
    def test_connection(cls, url, login, password):
        gi = galaxy.GalaxyInstance(url=url, email=login, password=password, verify=True)
        cls.check_status_code(gi.make_get_request(url=gi.base_url).status_code)

    def get_url_link_to_an_object(self, obj_type, obj_id):
        if obj_type == "history":
            url = os.path.join(self.url + f"history/switch_to_history?hist_id={obj_id}")
        elif obj_type == "data":
            url = self.url
        return url

    def get_data_root(self):
        gi = self.get_galaxy_instance()
        histories = []
        hi = gi.histories.get_histories()
        for h in hi:
            histories.append(
                ContainerDataObject(
                    id=h["id"],
                    name=h["name"],
                    inner_type="history",
                    url=self.get_url_link_to_an_object("history", h["id"])

                )
            )
        return histories

    def get_child_data(self, container_id):
        datas = []
        gi = self.get_galaxy_instance()
        di = gi.datasets.get_datasets(history_id=container_id)
        for d in di:
            if not d["deleted"]:
                ds = gi.datasets.show_dataset(dataset_id=str(d["id"]))
                size = ds["file_size"]
                datas.append(
                    ContainerDataObject(
                        id=d["id"],
                        name=d["name"],
                        inner_type="data",
                        url=self.get_url_link_to_an_object("data", d["id"]),
                        size=int(size))
                )
        return datas

    def download(self, object_id, path):
        gi = self.get_galaxy_instance()
        try:
            di = gi.datasets.show_dataset(dataset_id=object_id.strip())
            name = di["name"]
            gi.datasets.download_dataset(
                dataset_id=object_id.strip(),
                file_path=path + "/" + name,
                use_default_filename=False,
                require_ok_state=False,
            )
        except Exception as e:
            raise e

    def check_file_access(self, object_id):
        try:
            gi = self.get_galaxy_instance()
        except Exception as e:
            logger.debug(e)
            access = False
        try:
            gi.datasets.show_dataset(dataset_id=object_id.strip())
            access = True
        except Exception as e:
            logger.debug(e)
            access = False
        return access

    def get_space_info(self, objects_id, mapping_object=None):
        if mapping_object:
            jobid = utils.start_async_task()
        try:
            size = None
            gi = self.get_galaxy_instance()
            data = gi.datasets.show_dataset(dataset_id=objects_id[0])
            size = data["file_size"]
            if mapping_object:
                mapping_object.size = size
                mapping_object.save()
                utils.finish_async_task(jobid, size)
            else:
                return size
        except Exception as e:
            utils.error_async_task(jobid)
            return e

    def get_galaxy_instance(self):
        gi = galaxy.GalaxyInstance(
            url=self.url, email=self.login, password=self.password, verify=True
        )
        try:
            self.check_status_code(gi.make_get_request(url=gi.base_url).status_code)
        except Exception as e:
            raise e
        return gi


ToolConnector.register(GalaxyConnector)
