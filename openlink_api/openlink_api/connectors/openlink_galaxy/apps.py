from django.apps import AppConfig


class OpenlinkGalaxyConfig(AppConfig):
    name = "openlink_api.connectors.openlink_galaxy"
