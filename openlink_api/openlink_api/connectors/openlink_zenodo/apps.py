from django.apps import AppConfig


class OpenlinkZenodoConfig(AppConfig):
    name = "openlink_api.connectors.openlink_zenodo"
