import json
import logging
import os.path

import requests
from django import forms
from openlink_api.core.connector import (
    AuthentificationError,
    Publisher,
    ToolConnector,
    ToolForm,
    defaultError,
)

logger = logging.getLogger(__name__)


class ZenodoConnector(Publisher):
    def __init__(self, tool, token, user_vault_id=None):
        self.url = tool.get_public_param("url")
        self.token = tool.get_private_param("token", token, user_vault_id)
        self.tool = tool

    @classmethod
    def test_private_token(cls, url, private_token):
        r = requests.get(url, params={"access_token": private_token})
        cls.check_status_code(r.status_code)

    @classmethod
    def get_name(cls):
        return "Zenodo"

    @classmethod
    def get_type(cls):
        return "Publisher"

    @classmethod
    def has_access_url(cls):
        return True

    @classmethod
    def check_status_code(cls, r):
        if r != requests.codes.ok:
            if r == 401:
                raise AuthentificationError(cls, "token")
            else:
                raise defaultError(cls)

    @classmethod
    def get_logo(cls):
        logo = "images/logo-zenodo.png"
        return logo

    @classmethod
    def get_color(cls):
        color = "#7CD0DD"
        return color

    @classmethod
    def has_mapping_options(cls):
        return False

    def get_api_url(self):
        url_api = "%s/api/deposit/depositions" % (self.url)
        url_api = url_api.replace("//api", "/api")
        return url_api

    def get_url_link_to_an_object(self, obj_type, obj_id):
        # return a url to display for current user
        return self.url + "/record/" + obj_id.rsplit(".", 1)[1]

    def get_id(self, doi):
        logger.debug(doi.rsplit(".", 1))
        logger.debug(doi.split(".", 1))
        return doi.rsplit(".", 1)[1]

    def delete(self, obj_to_delete):
        # TODO
        # r = requests.delete('https://zenodo.org/api/deposit/depositions/1234',
        #            params={'access_token': ACCESS_TOKEN})
        return None

    def create_empty_depo(self):
        r = requests.post(
            self.get_api_url(),
            params={"access_token": self.token},
            json={},
            headers={"Content-Type": "application/json"},
        )
        return r.json()

    def add_file_to_depo(self, json, path_to_file):
        bucket_url = json["links"]["bucket"]
        filename = path_to_file.split("/")[-1]
        with open(path_to_file, "rb") as fp:
            requests.put(
                "%s/%s" % (bucket_url, filename),
                data=fp,
                params={"access_token": self.token},
            )
        return json

    def add_metadata_to_depo(self, meta, jsonr):
        r = requests.put(
            jsonr,
            params={"access_token": self.token},
            data=json.dumps(meta),
            headers={"Content-Type": "application/json"},
        )
        return r.json()

    def publish_depo(self, json):
        r = requests.post(
            json["links"]["publish"],
            params={"access_token": self.token},
        )
        return r

    def update_depo(self, doi, metadata):
        try:
            create_new_version = requests.post(
                self.get_api_url()
                + "/"
                + str(self.get_id(doi))
                + "/actions/newversion",
                {"access_token": self.token},
            )
            draft_to_update = create_new_version.json()["links"]["latest_draft"]
        except Exception:
            return ""
        update_metadata = requests.put(
            url=draft_to_update,
            headers={"Content-Type": "application/json"},
            params={"access_token": self.token},
            data=json.dumps(metadata),
        )
        pub_mod = update_metadata.json()["links"]["publish"]
        upload_path = "openlink/core/lib/tools/tmp"
        filename = str(metadata["metadata"]["title"]) + str(".zip")
        path_to_file = "%s/%s" % (upload_path, filename)
        with open(path_to_file, "rb") as fp:
            if filename == update_metadata.json()["files"][0]["filename"]:
                delete_file = requests.delete(
                    str(draft_to_update)
                    + "/files/"
                    + str(update_metadata.json()["files"][0]["id"]),
                    params={"access_token": self.token},
                    headers={"Content-Type": "application/json"},
                )
                logger.debug(str(delete_file.json))
            upload_file = requests.post(
                url=draft_to_update + "/files",
                params={"access_token": self.token},
                data={"name": filename},
                files={"file": fp},
            )
            logger.debug(str(upload_file.json))
        publish = requests.post(
            pub_mod,
            params={"access_token": self.token},
        )
        logger.debug(publish)
        os.remove(path_to_file)


ToolConnector.register(ZenodoConnector)
