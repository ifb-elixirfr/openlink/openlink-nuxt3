from django.apps import AppConfig


class OpenlinkSshConfig(AppConfig):
    name = "openlink_api.connectors.openlink_ssh"
