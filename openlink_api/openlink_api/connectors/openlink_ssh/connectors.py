import io
import logging
import os
import socket
from stat import S_ISDIR, S_ISREG
import openlink_api.core.connector
import paramiko
from django import forms
from openlink_api.core.connector import (
    ContainerDataObject,
    DataObject,
    Mapper,
    ToolConnector,
    ToolForm,
)

logger = logging.getLogger(__name__)


class SSHConnector(Mapper):
    def __init__(self, tool, token, user_vault_id=None):
        self.url = tool.get_public_param("url")
        self.username = tool.get_private_param("username", token, user_vault_id)
        self.private_key = tool.get_private_param("private_key", token, user_vault_id)
        self.tool = tool

    @classmethod
    def get_name(cls):
        return "SSH"

    @classmethod
    def get_type(cls):
        return "Mapper"

    @classmethod
    def get_data_structure(cls):
        return openlink_api.core.connector.TREE_STRUCTURE

    @classmethod
    def has_access_url(cls):
        return False

    @classmethod
    def get_logo(cls):
        logo = "images/logo-ssh.jpeg"
        return logo

    @classmethod
    def get_color(cls):
        color = "#C3C6C4"
        return color

    @classmethod
    def create_session(cls, username, hostname, private_key):
        client = paramiko.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        key_file = io.StringIO(private_key)
        key = paramiko.RSAKey.from_private_key(key_file)
        try:
            client.connect(hostname, username=username, pkey=key)
        except (
            paramiko.ssh_exception.BadHostKeyException,
            paramiko.ssh_exception.AuthenticationException,
            paramiko.ssh_exception.SSHException,
            socket.error,
        ) as ssh_exception:
            logger.debug("Warning: SSH connection error. (This could be temporary.)")
            logger.debug("Host: " + hostname)
            logger.debug("Error: " + ssh_exception)
        SFTP_client = client.open_sftp()
        return SFTP_client

    @classmethod
    def has_mapping_options(cls):
        return False

    def get_url_link_to_an_object(self, obj_type, obj_id):
        # return a url to display for current user
        return ""

    def get_space_info(self, objects_id, mapping_object=None):
        if mapping_object:
            jobid = utils.start_async_task()
        try:
            total_size = 0
            client = SSHConnector.create_session(self.username, self.url, self.private_key)
            for object_id in objects_id:
                entry = client.lstat(object_id)
                mode = entry.st_mode
                if S_ISREG(mode):
                    total_size += int(entry.st_size)
                if S_ISDIR(mode):
                    total_size += self.get_dir_size(client, object_id)
            if mapping_object:
                mapping_object.size = total_size
                mapping_object.save()
                utils.finish_async_task(jobid, total_size)
            else:
                return total_size
        except Exception as e:
            utils.error_async_task(jobid)
            return e

    def get_dir_size(self, client, dir_path):
        total_size = 0
        for file in client.listdir_attr(dir_path):
            mode = file.st_mode
            if S_ISREG(mode):
                total_size += file.st_size
            if S_ISDIR(mode):
                total_size += self.get_dir_size(
                    client, os.path.join(dir_path, file.filename)
                )
        return total_size

    def get_information(self, type, id):
        return {}

    def download(self, object_id, path):
        client = SSHConnector.create_session(self.username, self.url, self.private_key)
        entry = client.lstat(object_id)
        mode = entry.st_mode
        if S_ISREG(mode):
            name = object_id.rsplit("/")[-1]
            data_dir = os.path.join(path, name)
            client.get(remotepath=object_id, localpath=data_dir)
        if S_ISDIR(mode):
            name = object_id.rsplit("/")[-2]
            dir_path = os.path.join(path, name)
            try:
                os.chdir(dir_path)
            except OSError:
                os.mkdir(dir_path)
            self.download_dir(client, object_id, dir_path)
        client.close
        return None

    def download_dir(self, client, object_dir, dir_path):
        for file in client.listdir_attr(object_dir):
            remotepath = object_dir + "/" + file.filename
            localpath = os.path.join(dir_path, file.filename)
            mode = file.st_mode
            if S_ISDIR(mode):
                try:
                    os.chdir(localpath)
                except OSError:
                    os.mkdir(localpath)
                self.download_dir(client, remotepath, localpath)
            elif S_ISREG(mode):
                client.get(remotepath=remotepath, localpath=localpath)

    def check_file_access(self, object_id):
        client = SSHConnector.create_session(self.username, self.url, self.private_key)
        try:
            client.lstat(object_id)
            access = True
        except Exception as e:
            logging.debug(e)
            access = False
        client.close
        return access


ToolConnector.register(SSHConnector)
