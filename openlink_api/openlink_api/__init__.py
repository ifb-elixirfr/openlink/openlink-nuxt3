import os
import sys

__version__ = "1.0.2.rc8"


def manage():
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "openlink_api.settings")
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)